# Introduction
Workflow SMS notification module integrate Workflow module with SMS Framework
to send sms notifications on workflow state change.

# Dependencies
SMS: https://www.drupal.org/project/smsframework
Workflow: https://www.drupal.org/project/workflow

# Configuration

 1. Make sure your sms gateway integration is working fine.
 2. Add phone number settings in /admin/config/smsframework/phone_number
 3. Download and Enable the module.
 4. Navigate to /admin/config/workflow/workflow/YOUR_WORKFLOW/sms_notifications
 5. Add a new SMS notification.
 6. Done.

 ## Contact details
 Anas Mawlawi
 anas.mawlawi89@gmail.com
